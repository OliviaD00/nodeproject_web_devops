import { expect } from 'chai'
import { User, UserHandler } from '../src/controllers/user'
import { LevelDB } from "../src/leveldb"
import { doesNotReject } from 'assert'

const dbPath: string = 'db_test/users'
var dbUser: UserHandler

describe('Users', function () {
    before(() => {
        LevelDB.clear(dbPath)
        dbUser = new UserHandler(dbPath)
      })
    

    after(function () {
        dbUser.db.close()
    })

    describe('#get user', function () {
        it('should get empty array on non existing group', function (done) {
            dbUser.get("lalala", function (err: Error | null, result?: User) {
                expect(result).to.be.undefined || expect(result).to.be.empty
                done()
            })
        })
    })

    describe('#save user a new user and get date', function () {
        it('should save a new user', function (done) {
            var user: User = new User("...bis", "...bis@test.test", "test")
            dbUser.save(user, function (err: Error | null) {
                expect(err).to.not.be.null
                done()
            })
        })
        it('get data', function (done) {
            dbUser.get("...bis", function (err: Error | null, result?: User) {
                if (result) {
                    expect(result.username).to.equal("...bis")
                    expect(result.email).to.equal("...bis@test.test")
                    expect(result.getPassword()).to.equal("test")
                    done()
                }
            })
        })
    })

describe('#delete a user from the db', function () {
    it('should save a new user', function (done) {
        var user: User = new User("...bis1", "...bis@test.test", "test")
        dbUser.save(user, function (err: Error | null) {
            expect(err).to.not.be.null
            done()
        })
    })

    it('should delete data', function (done) {
        dbUser.delete("...bis1", function (err: Error | null, result?: User) {
            expect(err).to.be.null
        })
        done()
    })

    it('should get empty array on non existing group', function (done) {
        dbUser.get("...bis1", function (err: Error | null, result?: User) {
            expect(result).to.be.undefined || expect(result).to.be.empty
            done()
        })
    })
})
})
