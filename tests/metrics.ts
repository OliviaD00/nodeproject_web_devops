import { expect } from 'chai'
import { Metric, MetricsHandler } from '../src/controllers/metric'
import { LevelDB } from "../src/leveldb"

const dbPath: string = 'db_test/metrics'
var dbMet: MetricsHandler

describe('Metrics', function () {
    before(function () {
        LevelDB.clear(dbPath)
        dbMet = new MetricsHandler(dbPath)
    })

    after(function () {
        dbMet.db.close()
    })

    describe('#get metrics', function () {
        it('should get empty array on non existing group', function (done) {
            dbMet.getAllMetrics("...", function (err: Error | null, result?: Metric[]) {
                expect(err).to.be.null
                expect(result).to.not.be.undefined
                expect(result).to.be.empty
                done()
            })
        })
    })

    describe('#save and update metrics', function () {
        it('should save data', function (done) {
            var metric: Metric[] = []
            metric.push(new Metric(70, '1998-12-11 14:34', 10, "..."))
            dbMet.saveMetric(metric, function (err: Error | null, result?: null) {
            })
            done()
        })
        it('should get the data that is just saved', function (done) {
            dbMet.getOneMetric(70, "...", function (err: Error | null, result?: Metric[]) {
                expect(result).to.not.be.undefined
                if (result) {
                    expect(result[0].value).to.equal(10)
                    expect(err).to.be.null
                }
                done()
            })
        })

        it('should save the update', function (done) {
            var metric: Metric[] = []
            metric.push(new Metric(70, '1998-12-11 14:34', 20, "..."))
            dbMet.saveMetric(metric, function (err: Error | null, result?: null) {
            })
            done()
        })
        it('should get the updated data', function (done) {
            dbMet.getOneMetric(70, "...", function (err: Error | null, result?: Metric[]) {
                expect(result).to.not.be.undefined
                if (result) {
                    expect(result[0].value).to.equal(20)
                    expect(err).to.be.null
                }
                done()
            })
        })
    })

    describe('#delete metrics', function () {
        it('should delete all data', function (done) {
            dbMet.deleteAllMetrics("...", (err: Error | null) => {
                expect(err).to.be.null
            })
            done()
        })
        it('should get empty array on non existing group', function (done) {
            dbMet.getAllMetrics("...", function (err: Error | null, result?: Metric[]) {
                expect(err).to.be.null
                expect(result).to.not.be.undefined
                expect(result).to.be.empty
                done()
            })
        })
    })


    describe('#delete key metrics', function () {
        it('should not fail if data not exist data', function (done) {
            dbMet.deleteMetric(0, "...bis", (err: Error | null) => {
                expect(err).to.be.null
            })
            done()
        })
    })

    describe('#delete timestamp metrics', function () {
        it('should not fail if data not exist data', function (done) {
            dbMet.deleteMetricTime("2020-01-01 00:00", "...bis", (err: Error | null) => {
                expect(err).to.be.null
            })
            done()
        })
    })

})

