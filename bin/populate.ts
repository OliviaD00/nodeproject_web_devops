#!/usr/bin/env ts-node

import { Metric, MetricsHandler } from './../src/controllers/metric'
import { User, UserHandler } from './../src/controllers/user'

const met = [
  new Metric(1, '2019-04-03 14:32', 12, "Olivia"),
  new Metric(1, '2019-08-09 15:30', 92, "Olivia"),
  new Metric(1, '2019-11-12 10:23', 2, "Olivia"),
  new Metric(3, '2019-08-03 14:01', 32, "Olivia"),
  new Metric(3, '2019-10-09 15:33', 72, "Olivia"),
  new Metric(3, '2019-11-12 10:43', 2, "Olivia"),
  new Metric(3, '2018-02-02 13:43', 12, "test"),
  new Metric(3, '2017-02-02 13:73', 87, "test"),
  new Metric(2, '2017-08-09 18:33', 53, "test"),
  new Metric(2, '2017-11-12 23:59', 2, "test")
]

const users = [
  new User('Olivia', 'olivia.dalmasso@gmail.com', '1234'),
  new User('test', 'test@test.com', 'test'),
]

const db = new MetricsHandler('./db/metrics')
db.saveMetric(met, (err: Error | null) => {
  if (err) throw err
  console.log('Data for metrics populated')
})


const dbUser = new UserHandler('./db/users')

users.forEach((user: User) => {
  dbUser.save(user, (err: Error | null) => {
    if (err) throw err
    console.log('Data for user populated')
  })
}) 
