FROM node
RUN mkdir -p /usr/src/project
WORKDIR /usr/src/project
COPY package.json /usr/src/project/
RUN npm install
COPY . /usr/src/project
EXPOSE 8088
RUN npm run populate
RUN npm test
CMD npm start