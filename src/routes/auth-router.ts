import express = require('express')

//authorization
const authCheck = function (req: any, res: any, next: any) {
    if (req.session.loggedIn) {
      next()
    } else res.redirect('/login')
  }
//authentification routers
const authRouter = express.Router()

//redirecion
authRouter.get('/login', (req: any, res: any) => {
    res.render('login')
  })
  
  authRouter.get('/signup', (req: any, res: any) => {
    res.render('signup')
  })
  
  authRouter.get('/index', authCheck, (req: any, res: any) => {
    res.render('index')
  })
  
  authRouter.get('/data-user', authCheck, (req: any, res: any) => {
    res.render('data-user')
  })
  
  authRouter.get('/add-metric', authCheck, (req: any, res: any) => {
    res.render('add-metric')
  })
  
  authRouter.get('/delete-metric', authCheck, (req: any, res: any) => {
    res.render('delete-metric')
  })
  
  authRouter.get('/delete-metrics', authCheck, (req: any, res: any) => {
    res.render('delete-metric')
  })
  
  authRouter.get('/delete-metric-time', authCheck, (req: any, res: any) => {
    res.render('delete-metric-time')
  })

  authRouter.get('/display-metrics', authCheck, (req: any, res: any) => {
    res.render('display-metrics')
  })
  
  authRouter.get('/graph', authCheck, (req: any, res: any) => {
    res.render('graph')
  })
  
  authRouter.get('/graph-group', authCheck, (req: any, res: any) => {
    res.render('graph-group')
  })
  
  authRouter.get('/graph-one-by-one', authCheck, (req: any, res: any) => {
    res.render('graph-one-by-one')
  })
  
  authRouter.get('/signout', (req: any, res: any) => {
    delete req.session.loggedIn
    delete req.session.user
    res.redirect('/login')
  })

  export default authRouter