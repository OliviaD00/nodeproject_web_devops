import express = require('express')
import { Metric, MetricsHandler } from './controllers/metric'
import path = require('path')
import bodyparser = require('body-parser');
import session = require('express-session')
import levelSession = require('level-session-store')
import { UserHandler, User } from './controllers/user'
import authRouter from './routes/auth-router'

const dbUser: UserHandler = new UserHandler('./db/users')
const userRouter = express.Router()
const app = express();
const port: string = process.env.PORT || '8088'
const dbMet: MetricsHandler = new MetricsHandler('./db/metrics')
const LevelStore = levelSession(session)

app.use(express.static(path.join(__dirname, '/../public')))
app.use(bodyparser.json())
app.use(bodyparser.urlencoded())
app.set('views', __dirname + "/../views")
app.set('view engine', 'ejs');

//session
app.use(session({
  secret: 'my very secret phrase',
  store: new LevelStore('./db/sessions'),
  resave: true,
  saveUninitialized: true
}))
//authorization
const authCheck = function (req: any, res: any, next: any) {
  if (req.session.loggedIn) {
    next()
  } else res.redirect('/login')
}

// API server
app.use('/', authRouter)
// login the user if everything is ok
app.post('/login', (req: any, res: any, next: any) => {
  dbUser.get(req.body.username, (err: Error | null, result?: User) => {
    if (err)
      res.json({ "message": "user don't exist, go back" })
    if (result === undefined) {
      console.log("it will lead to an error")
    }
    else if (!result.validatePassword(req.body.password)) {
      res.render('error-password')
    } else {
      req.session.loggedIn = true
      req.session.user = result
      console.log('logged')
      res.redirect('/')
    }
  })
})
// save the user if the user not already exists
userRouter.post('/signup', (req: any, res: any, next: any) => {
  dbUser.get(req.body.username, function (err: Error | null, result?: User) {
    if (!err) console.log("User already exists")
    else if (result !== undefined) {
      console.log("User already exists")
    } else {
      dbUser.save(req.body, function (err: Error | null) {
        if (err) {
          next(err)
        }
        console.log("it is okay. User persisted")
        res.redirect('/login')
      })
    }
  })
})

// change password
app.post('/change-password', authCheck, (req: any, res: any, next: any) => {
  dbUser.get(req.session.user.username, function (err: Error | null, result?: any) {
    if (err || result == undefined) {
      if (err) throw err
    } else {
      result.setPassword(req.body.password)
      req.session.user.password = req.body.password
      dbUser.save(req.session.user, function (err: Error | null) { })
      next()
    }
  })

  res.redirect('index')
})

// change email 
app.post('/change-email', authCheck, (req: any, res: any, next: any) => {
  dbUser.get(req.session.user.username, function (err: Error | null, result?: any) {
    if (err || result == undefined) {
      if (err) throw err
    } else {
      result.setEmail(req.body.email)
      req.session.user.email = req.body.email
      dbUser.save(req.session.user, function (err: Error | null) { })
      next()
    }
  })
  res.redirect('index')
})



// API For metrics
// post one metric with the authentification 
app.post('/add-metric', authCheck, (req: any, res: any, next: any) => {
  const metrics: Metric[] = []
  console.log(req.body.timestamp)
  var date = req.body.timestamp.split("T")[0];
  var time = req.body.timestamp.split("T")[1];
  var timestamp = date + ' ' + time;
  const metric = new Metric(req.body.key, timestamp, req.body.value, req.session.user.username)
  console.log(metric)
  metrics.push(metric)
  dbMet.saveMetric(metrics, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
  res.redirect('/')
})
// post one metric with current time
app.post('/add-metric-current', authCheck, (req: any, res: any, next: any) => {
  const metrics: Metric[] = []
  var today = new Date();
  console.log(today)
  var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
  var time = today.getHours() + ":" + today.getMinutes();
  var timestamp = date + ' ' + time;
  const metric = new Metric(req.body.key, timestamp, req.body.value, req.session.user.username)
  console.log(metric)
  metrics.push(metric)
  dbMet.saveMetric(metrics, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
  res.redirect('/')
})
// delete a metric based on the key
app.post('/delete-metric', authCheck, (req: any, res: any, next: any) => {
  dbMet.deleteMetric(req.body.key, req.session.user.username, (err: Error | null) => {
    if (err) throw err
    console.log("the metric is deleted")
    res.redirect('index')
  })
})
// delete a metric based on the timestamp
app.post('/delete-metric-time', authCheck, (req: any, res: any, next: any) => {
  var date = req.body.timestamp.split("T")[0];
  var time = req.body.timestamp.split("T")[1];
  var timestamp = date + ' ' + time;
  dbMet.deleteMetricTime(timestamp, req.session.user.username, (err: Error | null) => {
    if (err) throw err
    console.log("the metric is deleted")
    res.redirect('index')
  })
})

//delete all metrics of a user
app.post('/delete-metrics', authCheck, (req: any, res: any, next: any) => {
  dbMet.deleteAllMetrics(req.session.user.username, (err: Error | null) => {
    if (err) throw err
    console.log("the metric is deleted")
    res.redirect('index')
  })
})

// user functions for API
userRouter.get('/:username', (req: any, res: any, next: any) => {
  dbUser.get(req.params.username, function (err: Error | null, result?: User) {
    if (err || result === undefined) {
      res.status(404).send("user not found. Please go back")
    } else res.status(200).json(result)
  })
})

// get all users
app.get('/user/all', (req: any, res: any) => {
  dbUser.getAllUsers((err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

// delete a user
userRouter.delete('/:username', (req: any, res: any) => {
  dbUser.delete(req.params.username, function (err: Error | null) {
    if (err) throw err
    else res.status(200).json({ "message": "user deleted" })
  }
  )
})

app.use('/user', userRouter)

app.get('/', authCheck, (req: any, res: any) => {
  res.render('index')
})

// get all metrics of a logged user
app.get('/getmetrics', authCheck, (req: any, res: any) => {
  dbMet.getAllMetrics(req.session.user.username, (err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

app.listen(port, (err: Error) => {
  if (err) {
    throw err
  }
  console.log(`server is listening on port ${port}`)
})

export {app, userRouter }