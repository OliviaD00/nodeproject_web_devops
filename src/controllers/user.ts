import { LevelDB } from "../leveldb"
import WriteStream from 'level-ws'
import { runInNewContext } from "vm"

const bcrypt = require ('bcrypt-nodejs')
//define class User and its functions
export class User {
  public username: string
  public email: string
  private password: string = ""

  constructor(username: string, email: string, password: string) {
    this.username = username
    this.email = email
    this.password = password
  }
  static fromDb(username: string, value: any): User {
    console.log(value)
    const [password, email] = value.split(":")
    return new User(username, email, password)
  }

  public setEmail(toSet: string): void {
    this.email = toSet
  }

  public setPassword(toSet: string) : void {
    this.password = toSet
  }

  public getPassword(): string {
    return this.password
  }

  //function to validate password
  public validatePassword(toValidate: string) {
    var hash = this.getPassword()
    var salt = bcrypt.genSaltSync(10)
    hash = bcrypt.hashSync(hash, salt);
    return bcrypt.compareSync(toValidate, hash)
  }

}

//API for USER
export class UserHandler {
  public db: any
  //have one user
  public get(username: string, callback: (err: Error | null, result?: User) => void) {
    this.db.get(`user:${username}`, function (err: Error, data: any) {
      if (err) {
        callback(err)
      }
      else if (data === undefined) {
        callback(null, data)
      }
      else callback(null, User.fromDb(username, data))
    })
  }

  //have all users
  public getAllUsers(callback: (error: Error | null, result?: any) => void) {
    const result: User[] = []
    this.db.createReadStream()
      .on('data', function (data: any) {
        const username: string = data.key.split(':')[1]
        const password: string = data.value.split(':')[0]
        const email: string = data.value.split(':')[1]
        const u: User = new User(username, email, password)
        console.log(u)
        //add to the list of users
        result.push(u)
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err, null)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null, result)
      })
  }

  // save one user
  public save(req: any, callback: (err: Error | null) => void) {
    var user = new User(req.username, req.email, req.password)
    this.db.put(`user:${user.username}`, `${user.getPassword()}:${user.email}`, (err: Error | null) => {
      callback(err)
    })
  }

  // delete one user
  public delete(username: string, callback: (err: Error | null) => void) {
    this.db.createReadStream()
      .on('data', (data: any) => {
        const usernameData: String = data.key.split(':')[1]
        if (usernameData === username) {
          this.db.del(data.key)
        }
      })
      .on('error', (err: Error) => {
        callback(err)
      })
      .on('end', () => {
        callback(null)
      })
  }
  //open an access to leveldb
  constructor(path: string) {
    this.db = LevelDB.open(path)
  }
}