import { LevelDB } from '../leveldb'
import WriteStream from 'level-ws'
import e from 'express'

export class Metric {

  public key: number
  public timestamp: string
  public value: number
  public username: string

  constructor(k: number, ts: string, v: number, u: string) {
    this.key = k // group of metric
    this.timestamp = ts
    this.value = v
    this.username = u
  }

  public getKey(): number {
    return this.key
  }
}

export class MetricsHandler {
  public db: any
  // give access to the db
  constructor(dbPath: string) {
    this.db = LevelDB.open(dbPath)
  }
  // save a metric
  public saveMetric(metrics: Metric[], callback: (error: Error | null) => void) {
    const stream = WriteStream(this.db)
    stream.on('error', callback)
    stream.on('close', callback)
    metrics.forEach((m: Metric) => {
      console.log(m)
      stream.write({ key: `metric:${m.key}:${m.username}:${m.timestamp}`, value: m.value })
    })
    stream.end()
  }
  //delete a metric based on its key
  public deleteMetric(key: number, username: string, callback: (error: Error | null) => void) {
    const rs = this.db
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp: string = data.key.split(':')[1]
        const id: number = parseInt(temp)
        const timestamp1: string = data.key.split(':')[3]
        const timestamp2: string = data.key.split(':')[4]
        const timestamp = timestamp1 + ":" + timestamp2
        const userID: string = data.key.split(':')[2]
        if ((id == key) && (userID === username)) {
          //delete
          console.log("key:" + id + "username :" + username + "timestamp:" + timestamp + ':' + data.value)
          console.log("it is delete")
          rs.del(data.key)
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null)
      })

  }

  // delete a metric with the returned key number, returned username, and returned timestamp
  public deleteMetricTime(timestamp: string, username: string, callback: (error: Error | null) => void) {
    const rs = this.db
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp: string = data.key.split(':')[1]
        const id: number = parseInt(temp)
        const time1: string = data.key.split(':')[3]
        const time2: string = data.key.split(':')[4]
        const time = time1 + ":" + time2
        const userID: string = data.key.split(':')[2]
        if ((userID === username) && (time === timestamp)) {
          //delete
          console.log("key:" + id + "username :" + username + "timestamp:" + timestamp + ':' + data.value)
          console.log("it is delete")
          rs.del(data.key)
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null)
      })
  }

  // delete a metric with the returned key number, returned username, and returned timestamp
  public deleteAllMetrics(username: string, callback: (error: Error | null) => void) {
    const rs = this.db
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp: string = data.key.split(':')[1]
        const id: number = parseInt(temp)
        const timestamp1: string = data.key.split(':')[3]
        const timestamp2: string = data.key.split(':')[4]
        const timestamp = timestamp1 + ":" + timestamp2;
        const userID: string = data.key.split(':')[2]
        if (userID === username) {
          //delete
          console.log("key:" + id + "username :" + username + "timestamp:" + timestamp + ':' + data.value)
          console.log("it is delete")
          rs.del(data.key)
          rs.del(data.value)
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null)
      })
  }
  // getAllMetrics with the returned username
  public getAllMetrics(username: string, callback: (error: Error | null, result: any) => void) {
    const result: Metric[] = []
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp: string = data.key.split(':')[1]
        const id: number = parseInt(temp)
        const userID: string = data.key.split(':')[2]
        const timestamp1: string = data.key.split(':')[3]
        const timestamp2: string = data.key.split(':')[4]
        const timestamp = timestamp1 + ":" + timestamp2;
        const m: Metric = new Metric(id, timestamp, data.value, userID)
        if (userID === username) {
          console.log(m)
          console.log("key:" + id + "username" + username + "timestamp:" + timestamp + ':' + data.value)
          //add to the list of metrics
          result.push(m)
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err, null)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null, result)
      })
  }


  //get one metric with the returned key number and username
  public getOneMetric(key: number, username: string, callback: (error: Error | null, result: any) => void) {
    const result: Metric[] = []
    this.db.createReadStream()
      .on('data', (data: any) => {
        const temp: string = data.key.split(':')[1]
        const id: number = parseInt(temp)
        const userID: string = data.key.split(':')[2]
        const timestamp1: string = data.key.split(':')[3]
        const timestamp2: string = data.key.split(':')[4]
        const timestamp = timestamp1 + ":" + timestamp2
        //if id is same as the key
        if ((id == key) && (userID == username)) {
          console.log("key:" + id + "username" + username + "timestamp:" + timestamp + ':' + data.value)
          const metric: Metric = new Metric(id, timestamp, data.value, userID)
          console.log(metric)
          result.push(metric)
        }
      })
      .on('error', (err: Error) => {
        callback(err, null)
      })
      .on('end', () => {
        callback(null, result)
      })
  }
}