# NodeProject_Web_DevOps

## Introduction

### Context
This is my projet for Technologies Web - Node.js
It is inspired of my previous weekly work for Technologies Web - Node.js. 
It is available here : https://gitlab.com/OliviaD00/git-ece-node.js
The project is a little Dashboard which :
* allows several users to acces to their own metrics 
* displays metrics in some graphs.
* uses simple API requests

### Documentation 
For doing my project, I used : 
* TypeScript documentation: [https://www.typescriptlang.org/docs/](https://www.typescriptlang.org/docs/)
* Mocha documentation: [https://mochajs.org/](https://mochajs.org/)
* D3.JS documentation: [http://d3js.org/]( http://d3js.org/)
* JQuery documentation: [https://code.jquery.com/](https://code.jquery.com/)
* Bootstrap documentation: [http://getbootstrap.com/](http://getbootstrap.com/)
* LevelDB documentation: [https://github.com/google/leveldb](https://github.com/google/leveldb)

### Bonus 
- group of metrics in the same graph or in separe graphs (two options)
- delete a group of metrics
- delete all metrics at once
- possibility to change the password
- possibility to change the email
- little animation of the graph (histogram) with all metrics
- hashed password but only when validate the password

### Issues and Limitations
Issues still present : 
- hash only with the validatePassword() locally

### For the future 
- put security to the project with https
- relieve server.ts from metrics and user routers. 

## Requirements 

### Node.js 

#### Operating systems 
Linux
SmartOS
Mac OSX 10.7 and higher
Windows Server 2008 and higher

## Installation 

### General
Start with `npm install` and launch the project with `npm start` (or `npm run dev`)  
IF YOU ARE NOT USING DOCKER PLEASE DELETE THE FOLDER DB BEFORE RUNNING THE PROJECT WITH `npm start` (issue)

### Tests

To start the tests (realized with Mocha) : `npm test`

### To populate

To populate users and metrics : `npm run populate`  
The project is initiated without data in the db.   
But I had an issue with Docker so I let the folder db. 

### Authentification (login/signup/signout)
- To signup
Go to http://localhost:8088/signup : you have to fill the form
If the username already exists it will not redirect you to login.
If everything is okay it will redirect you to http://localhost:8088/signup

- To login (first page of the project)
Go to http://localhost:8088/login : you have to fill the form.
If the password is not good it will redirect you to http://localhost:8088/error-password
If the user don't exist, it will displays an error in a JSON format  
Example with API : POST { "username":"test", "email":"test", "password" : "test" }

- To signout (log out)
Just click on logout it will redirect you to http://localhost:8088/login  
Or with API : GET http://localhost:8088/signout

### API 

- Information about users 
GET 
http://localhost:8088/user/all : will display all data about all users in the db
http://localhost:8088/user/<username> : will display data about a special user
If the user don't exist it will display an error

- To see all metrics of a user : 
GET 
http://localhost:8088/getmetrics

Good to know the user : "" with email = "" and password = "" is allowed in my project and cannot be deleted.

- To delete a user :
DELETE 
http://localhost:8088/user/<username>

### Front - End

You have several options with several pages. A lot of informations is displayed on front-end pages.
You can of course click on buttons to navigate betweens pages. 
Homepage is index.ejs for my project.
#### To add a metric :
After authentification, submit all lines in the front-end 
http://localhost:8088/add-metric   
Example with the API : POST http://localhost:8088/add-metric when logged as Olivia
{ "key": 2, "timestamp":"2012-12-12 12:12" , "value" : 3, "username" :"Olivia" }

You have two choices : add all by hand or add metrics with current timestamp
Key is only for groups of metrics and it is not mandatory. 

#### To update a metric :
Go to http://localhost:8088/add-metric when logged and post key and timestamp and update the value

#### To delete a metric :
After authentification, submit the line in the front-end : 
http://localhost:8088/delete-metric

You have three choices : 
* delete a key (group of metrics) 
* delete a specific timestamp
* delete all metrics of the user
Example with the API : POST http://localhost:8088/delete-metrics 
POST http://localhost:8088/delete-metric {"key" : 0}

#### To see all metrics of a user : 
After authentification, click on the button or go to : 
http://localhost:8088/display-metrics and click to <Bring all the metrics !>

It will display some ALERTS for each metric.    
Then you will have the result writing on the page.

#### To see all metrics on a graph :
Click on the button or go to :
http://localhost:8088/graph (histogram)
Here data are not seperated by the value of the variable key (group of metrics)

#### To see metrics of a special key of the user (group of metrics) :
A group of metric is metrics grouped by a variable key.  
We can imagine that key is a name of a set of metrics (like temperature, sleeptime, money variation).  
In the dashboard you can see these several group of metrics on the same graph and on several graphs.

After authentification, click on the button or go to :
http://localhost:8088/graph-group to see all group of metrics in a single graph (line chart)  
Then go to http://localhost:8088/graph-one-by-one to see each group of metrics individually (several line chart) 

#### To change password or email :
After authentification, click on the button or go to :
http://localhost:8088/data-user and fill the form.   
Example with the api http://localhost:8088/change-password  
POST { "password" : "Olivia"}   
Example with the api http://localhost:8088/change-email  
POST { "email" : "Olivia@olivia.com"} 

### Test limitations of isolation :
Isolation is maintained with authCheck.

#### Insert metrics unlogged :
GET http://localhost:8080/getmetrics : it will redirect to the page of login 

#### Try reading metrics unlogged :
POST http://localhost:8088/add-metric   
{ "key": 2, "timestamp":"2012-12-12 12:12" , "value" : 3, "username" : "Olivia" }
it will redirect to the page of login 

#### Login and try reading someone else's metrics :
GET http://localhost:8088/getmetrics with authCheck a user can only access to his metrics (there is no post method)

#### Login and try adding someone else's metrics :
POST http://localhost:8088/add-metric when logged as test for instance with authCheck a user can not add metrics of others users 
{ "key": 2, "timestamp":"2012-12-12 12:12" , "value" : 3, "username" :"Olivia" }

## DevOps

### Git
Three branches : (two for development, on master). Tags with versions. See below.

### Tests
Unit tests for user and for metrics with mocha and chai.

### Docker
One Dockerfile for packaging the project (build, populate, test and start)

Command to build :
- docker build -t nodeproject . (if you are in the right repository)

Command to run : 
- docker run -p 8088:8088 nodeproject

A .dockerignore is added to ignore the dockerfile when clonning the repository.

### Git-lab CI 

Has two stages : 
- build for npm install
- test for npm test

### Difficulties and improvements
Some difficulties on writing good tests (reflexions). 
Pipeline failed a lot of times too during dev.

#### v1.0.0 Issues

- delete (linked to the user) don't work yet
- delete user don't work
- trying to loggin with a not found user don't work (lead to an error)
- some tests have still an error
- no graph
- problems with populate.ts

#### v1.1.0
- delete (linked to the user) don't work yet
- delete user don't work
- trying to loggin with a not found user don't work (lead to an error)
- some tests have still an error
- problems with populate.ts, will be deleted

#### v1.1.1
- delete user don't work
- trying to loggin with a not found user don't work (lead to an error)
- some tests have still an error
- re-adding populate.ts

#### v1.1.2
- delete user don't work
- trying to loggin with a not found user don't work (lead to an error)
- some tests have still an error
- re-adding populate.ts
- But WE HAVE A GRAPH

#### v1.1.3
- no major bugs

#### v1.1.4
- fixing and adding some little options (current time)
- adding some bonus (group of metrics, change password, change email)
- adding animation for the front part with d3.js 
- apply best practices for the project (for instance : kebab-case, camelCase)
- add bcrypt for the function validatePassword
- adding delete a timestamp metric
- adding delete all metrics of a user

#### v1.1.5
- fixing some bugs 

### Others

Add MIT Licence. 
Add badge "pipeline passed".

### For the future

We can think of adding new applications.
For doing that we can implement an automatisation of installation. 
#### Nifi 
Nifi can help to import directly some data in a csv format or example or local database.
For example if we want to install Nifi : 
- Run on a different docker windows this command : 
`Docker run -p 8080:8080 -v <path>\.docker\nifi\<input>:/<input> -ti apache/nifi` 
- Link it to the project with a docker-compose.yml like this and start with the command `docker-compose up` :   
***version: '3'  
***services:    
    ******nodeproject:  
        *********build : .  
        *********ports :  
        *********- "8088:8088"    
    ******nifi:  
        *********image: apache/nifi  
        *********ports:  
        *********- "8080:8080"  

"***" is for indentation.
I had an issue with my docker-compose.yml, so I delete it. 
I didn't put a path for the volume in the docker-compose.yml because the project is not ready yet to accept volume.
Moreover, we may have to allow Docker to access to local data of the host.
I admit there is still some configurations to do before running Nifi, but at least I tried.

## Contributor

Name : Olivia Dalmasso
Group : ING4 TD01